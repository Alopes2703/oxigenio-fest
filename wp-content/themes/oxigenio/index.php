<?php
/* Template name: Home */

get_header();

?>

<main>

    <a class="logo-header" href="<?php echo esc_url( home_url( '/' ) ); ?>">
        <h1>Oxigênio Festival</h1>
    </a>    

    <section id="home" class="home" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/bg-home.png)">

        <div class="wrap">

            <h2>Vans</h2>
            <p class="presents">Apresenta:</p>
            <figure>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/lineup-middle-3.png)" alt="Lineup Oxigênio">
                <img class="bands" src="<?php echo get_template_directory_uri(); ?>/assets/images/lineup-border-3.png)" alt="Lineup Oxigênio">
            </figure>
            <a href="#about" class="page-scroll scroll">scroll</a>
        </div>

        
    </section>

    <section class="tickets" id="about">
        <a href="#home" class="page-scroll back-to-lineup">lineup</a>
        <div class="top">
            <div class="container container-rel">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-9">
                        <div class="date">13, 14 e 15<span>de setembro</span></div>
                        <h2>Oxigênio Festival</h2>
                        <h3>Chegamos a <strong>sexta edição</strong></h3>
                        <p>Oxigênio Festival chega à sua sexta edição em 2019.<br>Um dos eventos mais plurais no ramo<br>do entretenimento nacional.</p>
                        <p class="participe">Participe dessa experiência<span>tickets</span></p>
                    </div>
                </div>
                <figure class="bg-edition">
                    <img class="six animated fadeInRight faster wow" data-wow-delay="1.3s" src="<?php echo get_template_directory_uri(); ?>/assets/images/6.png)" alt="Sexta Edição">
                    <img class="astr animated zoomIn faster wow" data-wow-delay="1.6s" src="<?php echo get_template_directory_uri(); ?>/assets/images/astronaut.png)" alt="Little Astronaut">
                </figure>
            </div>
        </div>
        
        <div class="bottom" id="tickets">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-6">
                        <h3>garanta já o <strong>seu ingresso</strong></h3>
                        <p><strong>passaporte para os<br> 3 dias de evento</strong><br> ou unitário por dia<br> de evento.</p>
                    </div>
                    <div class="col-12 col-md-12 col-lg-6 col-right">
                        <div class="inner">
                            <div class="head">
                                <p>dias 13, 14 e 15 de setembro</p>
                                <p>compre online</p>
                            </div>
                            <small>tickets</small>
                            <div class="body">
                                <div class="line">
                                    <p>passaporte 3 dias de evento: <strong>R$ 180,00 (meia-promo)</strong></p>
                                    <a target="_blank" href="https://pixelticket.com.br/eventos/3724/oxigenio-festival-2019" class="btn btn-custom blue">Comprar</a>
                                </div>
                                <div class="line">
                                    <p>Unitário por dia de evento: <strong>R$ 70,00 (meia-promo)</strong></p>
                                    <a target="_blank" href="https://pixelticket.com.br/eventos/3724/oxigenio-festival-2019" class="btn btn-custom blue">Comprar</a>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>


    </section>

    <section class="instagram" id="experience">
        <h2 class="animated fadeInUp faster wow" data-wow-delay=".5s">instagram: <strong>@oxigeniofestival</strong></h2>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 animated fadeInUp faster wow" data-wow-delay="1s">
                    <div class="card-gallery image-gallery" id="instafeed"></div>
                    <p>confira o que já rolou<br>e o que vai rolar</p>
                </div>
            </div>
        </div>
        <small>13.14.15/09</small>
    </section>

    <section class="spotify">
        <div class="top">
            <div class="row">
                <div class="col-12">
                    <h3 class="animated fadeInUp faster wow" data-wow-delay=".5s">ouça a playlist oficial do</h3>
                    <h2 class="animated fadeInUp faster wow" data-wow-delay="1s">OxigÊnio festival 2019</h2>
                </div>
            </div>
        </div>
        <div class="playlist">
            <div class="top"></div>
            <div class="body"></div>
            <div class="wrap-framwe">
                <iframe src="https://open.spotify.com/embed/playlist/0ZjNnbULh5Ul7z3xVF7l5l" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
            </div>
            
        </div>
        
    </section>
    
    <section class="experience" >
        <div class="top">
            <h2>experience</h2>
            <div class="ox animated zoomIn faster wow" data-wow-delay=".5s">
                <svg width="177" height="91" viewBox="0 0 177 91" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M47.4786 0.129992C21.8686 0.129992 0.808594 20.54 0.808594 45.5C0.808594 70.46 21.8686 91 47.4786 91C73.0886 91 94.1486 70.46 94.1486 45.5C94.1486 20.54 73.0886 0.129992 47.4786 0.129992ZM47.4786 2.73C71.6586 2.73 91.5486 21.97 91.5486 45.5C91.5486 69.03 71.6586 88.4 47.4786 88.4C23.2986 88.4 3.40859 69.03 3.40859 45.5C3.40859 21.97 23.2986 2.73 47.4786 2.73ZM107.455 0.129992L138.915 43.94L104.985 91H108.105L140.475 46.02L172.975 91H176.095L142.035 43.94L173.625 0.129992H170.505L140.475 41.73L110.575 0.129992H107.455Z" fill="#1C0E3A" fill-opacity="0.2"/>
                </svg>
            </div>
        </div>
        <div id="karaoke" class="bottom" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/bg-blur-karaoke.png)">
            <img class="ox-left" src="<?php echo get_template_directory_uri(); ?>/assets/images/ox-xp-left.png)" alt="Freddy Mercury">
            <img class="ox-right" src="<?php echo get_template_directory_uri(); ?>/assets/images/ox-xp-right.png)" alt="Freddy Mercury">
            <div class="container container-rel">
                <div class="row">
                    <div class="col-12 animated fadeInUp faster wow" data-wow-delay=".5s">
                        <h2>você no palco <strong>do oxigênio</strong></h2>
                        <div class="wrap">
                            <p>Escolha a sua música, cadastre-se e concorra ao seu minuto de fama no palco do <strong>Oxigênio Festival 2019.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-lg-8 images">
                        <img class="big-astr" src="<?php echo get_template_directory_uri(); ?>/assets/images/big-astronaut.png)" alt="Freddy Mercury">
                        <img class="karaoke" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-karaoke.png)" alt="Freddy Mercury">
                    </div>
                </div>
                <?php echo do_shortcode( '[contact-form-7 id="5" title="Form Karaoke"]' ); ?>
                <h3 class="soon">Em Breve</h3>
                <div class="row">
                    <div class="col-12 col-lg-6">

                    </div>
                    <div class="col-12 col-lg-6">
                        <small>Selecione um dia ou mais</small>
                        
                    </div>
                </div>
                <img class="freddy" src="<?php echo get_template_directory_uri(); ?>/assets/images/freddy.png)" alt="Freddy Mercury">
            </div>

        </div>
    </section>

    <section class="your-band" id="banda" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/bg-band.png)">
        <div class="container">
            <div class="row">
                <div class="col-12 animated fadeInUp faster wow" data-wow-delay=".5s">
                    <h2>sua banda no festival</h2>
                </div>
            </div>

            <!-- Comentar o Bloco Cadastro -->

            <!-- <div class="bloco-cadastro">
                <div class="row align-items-center">
                    <div class="col-12 col-lg-6">
                        <h3>cadastre a<span>sua banda</span></h3>
                    </div>
                    <div class="col-12 col-lg-6">
                        <img  src="<?php echo get_template_directory_uri(); ?>/assets/images/palco-vans.png)" alt="Show">
                    </div>
                    <div class="col-12 col-lg-12 col-custom">
                        <p>e concorra <strong class="space">a uma vaga</strong><br>para banda <strong >de abertura</strong></p>
                        <div class="block-cadastro">
                            <div class="arrow-down">
                                <svg width="27" height="35" viewBox="0 0 27 35" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <rect x="2" y="0.000488281" width="18.3842" height="2.82834" rx="1.41417" transform="rotate(45 2 0.000488281)" fill="#141414" fill-opacity="0.3"/>
                                    <rect x="26.0352" y="2" width="18.3842" height="2.82834" rx="1.41417" transform="rotate(135 26.0352 2)" fill="#141414" fill-opacity="0.3"/>
                                    <rect x="2" y="20.0005" width="18.3842" height="2.82834" rx="1.41417" transform="rotate(45 2 20.0005)" fill="#141414" fill-opacity="0.3"/>
                                    <rect x="26.0352" y="22" width="18.3842" height="2.82834" rx="1.41417" transform="rotate(135 26.0352 22)" fill="#141414" fill-opacity="0.3"/>
                                </svg>
                            </div>
                            <a href="javascript:void" class="show-form">Cadastre-se</a>
                        </div>
                    </div>
                    <div class="col-12 hidden-form">
                        <?php echo do_shortcode( '[contact-form-7 id="11" title="Form Bandas"]' ); ?>
                    </div>
                </div>

                <style>
                    .bloco-cadastro {
                        display: block;
                    }

                    .bloco-encerrado {
                        display: none;
                    }
                
                </style>
            </div> -->



            <div class="bloco-encerrado">
                <h3>Inscrições encerradas</h3>
                <p>Acompanhe a página do nosso evento no facebook<br>e fique ligado nas bandas selecionadas para a votação do público. </p>
                <h4>Boa sorte</h4>
                <div class="ox-ex">
                    <h2>...ox.experience</h2>
                </div>
                <div class="palco-vans">
                    <h2>palco vans off the wall</h2>
                    <p>oxigênio festival 2018</p>
                </div>
            </div>
        </div>
        
    </section>

    <section class="partners" id="informacoes">
        <div class="container">
            <small>patrocínio</small>
            <div class="row">
                <div class="col-12 col-custom">
                    <div class="slide-logos">
                        <a target="_blank" href="https://www.vans.com.br/">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-vans.png)" alt="Logo">
                        </a>
                        <a target="_blank" href="https://www.monsterenergy.com/">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-monster.png)" alt="Logo">
                        </a>
                        <a target="_blank" href="https://www.gooseislandsp.com/">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-goose.png)" alt="Logo">
                        </a>
                        <a target="_blank" href="https://www.budweiser.com.br/">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-bud.png)" alt="Logo">
                        </a>
                        <a target="_blank" href="https://www.jackdaniels.com/">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-jackdaniels.png)" alt="Logo">
                        </a>
                        <!-- <a target="_blank" href="http://www.brutalkill.com.br/">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-brutalkill.png)" alt="Logo">
                        </a>
                        <a target="_blank" href="https://cabify.com/pt-BR">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-cabify.png)" alt="Logo">
                        </a>
                        <a target="_blank" href="https://www.spotify.com/br/">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-spotify.png)" alt="Logo">
                        </a>
                        <a target="_blank" href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-hangar.png)" alt="Logo">
                        </a>
                        <a target="_blank" href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-gig.png)" alt="Logo">
                        </a> -->
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="place">
        <div class="wrap">
            <h2 class="animated fadeInUp faster wow" data-wow-delay=".5s">VIA matarazzo</h2>
            <h3 class="animated fadeInUp faster wow" data-wow-delay=".5s">espaço de eventos</h3>
            <p class="animated fadeInUp faster wow" data-wow-delay=".5s">Av. Francisco Matarazzo, 764 - Água Branca, São Paulo - SP</p>
            <p class="animated fadeInUp faster wow"><strong>contato: <a href="mailto:contato@contato@gigmusic.com.br" target="_blank" data-wow-delay=".5s">contato@gigmusic.com.br</strong></a></p>
        </div>
        <div class="realizacao">
            <p class="animated fadeInUp faster wow" data-wow-delay=".5s">realização</p>
            <div class="wrap animated fadeInUp faster wow" data-wow-delay=".5s">
                <a target="_blank" href="javascript:void(0)">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-hangar-trans.png)" alt="Logo">
                </a>
                <a target="_blank" href="javascript:void(0)">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-gig-trans.png)" alt="Logo">
                </a>
            </div>
        </div>
    </section>



</main>





<?php get_footer(); ?>


<script type="text/javascript">

    particlesJS.load('home', '<?php echo get_template_directory_uri(); ?>/assets/js/particlesjs-config.json', function() {
        console.log('callback - particles.js config loaded');
    });
	
    // Instagram

    if(jQuery(window).width()>768){


        var feed = new Instafeed({
            get: 'user',
            userId: '5628164782',
            clientId: '	8489476dc2924356bfd6836b2b4b762e',
            accessToken:'5628164782.1677ed0.9b3f8ff2088d445890a1e412756fe41d',
            resolution: 'low_resolution',
            template: '<a href="{{link}}" class="popup-image mb-0"><img class="animated" alt="" src="{{image}}" ></a>',
            //template: '<div class="col-6 col-md-4 col-lg-4 col-custom"><a href="{{link}}"><img src="{{image}}" /></a></div>',
            limit: 7,
        });
        
        feed.run();

    }

    // if(jQuery(window).width()>499){


    //     var feed = new Instafeed({
    //         get: 'user',
    //         userId: '5628164782',
    //         clientId: '	8489476dc2924356bfd6836b2b4b762e',
    //         accessToken:'5628164782.1677ed0.9b3f8ff2088d445890a1e412756fe41d',
    //         resolution: 'low_resolution',
    //         template: '<a href="{{link}}" class="popup-image mb-0"><img class="animated" alt="" src="{{image}}" ></a>',
    //         //template: '<div class="col-6 col-md-4 col-lg-4 col-custom"><a href="{{link}}"><img src="{{image}}" /></a></div>',
    //         limit: 4,
    //     });

    //     feed.run();

    // }

    if(jQuery(window).width()<499){


        var feed = new Instafeed({
            get: 'user',
            userId: '5628164782',
            clientId: '	8489476dc2924356bfd6836b2b4b762e',
            accessToken:'5628164782.1677ed0.9b3f8ff2088d445890a1e412756fe41d',
            resolution: 'low_resolution',
            template: '<a href="{{link}}" class="popup-image mb-0"><img class="animated" alt="" src="{{image}}" ></a>',
            //template: '<div class="col-6 col-md-4 col-lg-4 col-custom"><a href="{{link}}"><img src="{{image}}" /></a></div>',
            limit: 2,
        });

        feed.run();

    }

 </script>










