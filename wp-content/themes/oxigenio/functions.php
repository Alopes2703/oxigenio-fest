<?php

//Add CSS/JS

add_action('wp_enqueue_scripts', 'styles_init');
add_action('wp_enqueue_scripts',  'js_init');

function js_init() {
    //wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery-3.2.1.min.js', array(), '', true );
    wp_enqueue_script( 'popper', get_template_directory_uri() . '/assets/js/popper.min.js', array( 'jquery' ), '', true );
    wp_enqueue_script( 'js-bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array( 'jquery' ), '', true );
    wp_enqueue_script( 'slick', get_template_directory_uri() . '/assets/js/slick.js', array( 'jquery' ), '', true );
    wp_enqueue_script( 'mask', get_template_directory_uri() . '/assets/js/jquery.mask.min.js', array( 'jquery' ), '', true );
    wp_enqueue_script( 'wow', get_template_directory_uri() . '/assets/js/wow.min.js', array( 'jquery' ), '', true );
    wp_enqueue_script( 'instagram', get_template_directory_uri() . '/assets/js/instafeed.min.js', array( 'jquery' ), '', true );
    wp_enqueue_script( 'particles', get_template_directory_uri() . '/assets/js/particles.min.js', array( 'jquery' ), '', true );
    wp_enqueue_script( 'script', get_template_directory_uri() . '/assets/js/script.js', array( 'jquery' ), '', true );

}

function styles_init() {
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css');
    wp_enqueue_style( 'fotn-awesome', get_template_directory_uri() . '/assets/css/all.min.css');
    wp_enqueue_style( 'normalize', get_template_directory_uri() . '/assets/css/normalize.css' );
    wp_enqueue_style( 'slick', get_template_directory_uri() . '/assets/css/slick.css');
    wp_enqueue_style( 'animate', get_template_directory_uri() . '/assets/css/animate.css');
    wp_enqueue_style( 'style', get_template_directory_uri() . '/assets/css/style.min.css');
}


// Logo Admin
function cutom_login_logo() {
	echo "<style type=\"text/css\">
	body.login div#login h1 a {
		background-image: url(".get_bloginfo('template_directory')."/assets/images/logo.png);
		-webkit-background-size: auto;
		background-size: 100%;
		margin: 0 auto 25px auto;
        width: 55%;
        height: 180px;
    	display: table;
    }

    body.login {
        -webkit-background-size: cover;
		background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
        background-color: #1e272e!important;
        background-image: url(".get_bloginfo('template_directory')."/assets/images/bg-admin.png);
        position: relative;
    }

    .login form {
        background: #A70E13!important;
        border-radius: 40px 0;
    }

    .login label {
       color: #fff!important;
    }

    .wp-core-ui .button-primary {
        background: rgba(130, 0, 0, 0.78)!important;
        box-shadow: none!important;
        color: #fff;
        text-decoration: none;
        text-shadow: none!important;
        border: none!important;
        transition: all 0.3s ease-out 0s!important;
    }

    .wp-core-ui .button-primary:hover {
        background: rgba(130, 0, 0, 0.5)!important;
        transition: all 0.3s ease-out 0s!important;
    }

    .login #backtoblog a, .login #nav a {
        text-decoration: none;
        color: #fff!important;
    }

    
    
</style>";
}
add_action( 'login_enqueue_scripts', 'cutom_login_logo' );

add_action('wp_head', 'change_bar_color');
add_action('admin_head', 'change_bar_color');
function change_bar_color() {
    echo "<style type=\"text/css\">
	#wpadminbar{
        background: #A70E13 !important;
    }

    #adminmenu, #adminmenu .wp-submenu, #adminmenuback, #adminmenuwrap {
        background-color: #A70E13!important;
    }

    #adminmenu li.menu-top:hover, #adminmenu li>a.menu-top:focus {
        background-color: #A70E13!important;
        color: #fff!important;
    }

    #adminmenu .wp-submenu a:focus, #adminmenu .wp-submenu a:hover, #adminmenu a:hover, #adminmenu li.menu-top>a:focus {
        color: #fff!important;
    }

    #wpadminbar .menupop .ab-sub-wrapper, #wpadminbar .shortlink-input {
        background: #A70E13!important;
    }

    #wpadminbar .quicklinks .menupop ul.ab-sub-secondary, #wpadminbar .quicklinks .menupop ul.ab-sub-secondary .ab-submenu {
        background: #A70E13!important;
    }

    #wpadminbar:not(.mobile) .ab-top-menu>li:hover>.ab-item, #wpadminbar:not(.mobile) .ab-top-menu>li>.ab-item:focus {
        background: #A70E13!important;
    }

    

    
    
</style>";
}

function htx_custom_logo() {
    echo '
    <style type="text/css">
    #wpadminbar #wp-admin-bar-wp-logo>.ab-item { 
    background-image: url(' . get_bloginfo('stylesheet_directory') . '/assets/images/favicon.png) !important; 
    background-position: center!important;
    width: 40%!important;
    background-size: 100%!important;
    background-repeat: no-repeat!important;
    margin: 5px!important;
    }
    #wpadminbar #wp-admin-bar-wp-logo.hover > .ab-item .ab-icon {
    background-position: 0 0;
    }

    #wpadminbar #wp-admin-bar-wp-logo>.ab-item .ab-icon:before {
         content: none;
    }

     </style>
    ';
    }
    
     //hook into the administrative header output
    add_action('admin_head', 'htx_custom_logo');


//Remove Admin Bar
function my_function_admin_bar(){
    return false;
}
add_filter( 'show_admin_bar' , 'my_function_admin_bar');

//Menus
register_nav_menus( array(
    'institucional' => __(  'Menu Institucional' , 'CAP'),
    'mapa-do-site_cap'  => __( 'Footer CAP', 'cap') ,
    
) );

 /*
 * Let WordPress manage the document title.
 * By adding theme support, we declare that this theme does not use a
 * hard-coded <title> tag in the document head, and expect WordPress to
 * provide it for us.
 */
add_theme_support('title-tag');


/* Tamanhos customizados */
add_image_size('banner', 1920, 741, true);
add_image_size('banner-mobile', 375, 667, true);
add_image_size('thumb-cap', 370, 438, true);
add_image_size('thumb-evento-principal', 775, 775, true);
add_image_size('thumb-evento-home', 381, 381, true);
add_image_size('thumb-noticia-home', 267, 387, true);
add_image_size('poster-cinema', 241, 320, true);




add_action('init', 'cptui_register_cat_funcao');
function cptui_register_cat_funcao() {

/**
 * Taxonomy: Funções.
 */

    $labels = array(
        "name" => __( "Funções", "" ),
        "singular_name" => __( "Função", "" ),
        "menu_name" => __( "Funções", "" ),
        "all_items" => __( "Todas as funções", "" ),
        "edit_item" => __( "Editar função", "" ),
        "view_item" => __( "Ver função", "" ),
        "update_item" => __( "Atualizar função", "" ),
        "add_new_item" => __( "Add nova função", "" ),
    );

    $args = array(
        "label" => __( "Funções", "" ),
        "labels" => $labels,
        "public" => true,
        "hierarchical" => true,
        "label" => "Funções",
        "show_ui" => true,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "query_var" => true,
        "rewrite" => array( 'slug' => 'cat_funcao', 'with_front' => true, ),
        "show_admin_column" => true,
        "show_in_rest" => true,
        "rest_base" => "cat_funcao",
        "show_in_quick_edit" => true,
    );
    register_taxonomy( "cat_funcao", array( "noticias", "eventos" ), $args );
}

function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function wpb_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;    
    }
    wpb_set_post_views($post_id);
}
add_action( 'wp_head', 'wpb_track_post_views');





// Alter search posts per page
function myprefix_search_posts_per_page($query) {
    if ( $query->is_search ) {
        $query->set( 'posts_per_page', '6' );
    }
    return $query;
}
add_filter( 'pre_get_posts','myprefix_search_posts_per_page' );



// Remove 'Posts' from Sidebar Menu

function remove_menu () 
{
   remove_menu_page('edit.php');
} 

add_action('admin_menu', 'remove_menu');



/* ==========================================================================
 *  Adicionar Configuracões Gerais
 * ========================================================================== */
acf_add_options_page(array(
    'page_title'    => 'Configurações do tema "CAP"',
    'menu_title'    => 'Opções',
    'menu_slug'     => 'cap-theme-options',
    'capability'    => 'edit_posts',
    'position'      => 4
));


acf_add_options_sub_page(array(
    'page_title'    => 'Configurações',
    'menu_title'    => 'Redes Sociais ',
    'parent_slug'   => 'cap-theme-options',
));

acf_add_options_sub_page(array(
    'page_title'    => 'Configurações',
    'menu_title'    => 'Infos Gerais',
    'parent_slug'   => 'cap-theme-options',
));


/* ==========================================================================
 *  Adcionar Thumbnails
 * ========================================================================== */

add_theme_support('post-thumbnails', array('post','blog','page','share', 'noticias', 'cursos', 'eventos'));



/* ==========================================================================
 *  Limitar Caracteres Excerpt
 * ========================================================================== */
function excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt)>=$limit) {
      array_pop($excerpt);
      $excerpt = implode(" ",$excerpt).'...';
    } else {
      $excerpt = implode(" ",$excerpt);
    }	
    $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
    return $excerpt;
  }

  function excerpt_title($limit) {
    $excerpt_title = explode(' ', get_the_title(), $limit);
    if (count($excerpt_title)>=$limit) {
      array_pop($excerpt_title);
      $excerpt_title = implode(" ",$excerpt_title).'...';
    } else {
      $excerpt_title = implode(" ",$excerpt_title);
    }	
    $excerpt_title = preg_replace('`[[^]]*]`','',$excerpt_title);
    return $excerpt_title;
  }

  function excerpt_content($limit) {
    $excerpt_content = explode(' ', get_the_content(), $limit);
    if (count($excerpt_content)>=$limit) {
      array_pop($excerpt_content);
      $excerpt_content = implode(" ",$excerpt_content).'...';
    } else {
      $excerpt_content = implode(" ",$excerpt_content);
    }	
    $excerpt_content = preg_replace('`[[^]]*]`','',$excerpt_content);
    return $excerpt_content;
  }


  // Remove AutoP Contact Form

  add_filter('wpcf7_autop_or_not', '__return_false'); 








?>