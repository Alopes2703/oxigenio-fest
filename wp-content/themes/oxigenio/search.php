<?php

/* Template name: Template Busca*/

get_header();

?>

<main class="inner search-results">

   
    <!-- <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-12 title animated fadeInUp wow" data-wow-delay="1s">
                    <h2>Resultado da Busca</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <p>
						<?php $allsearch = new WP_Query("s=$s&showposts=-1");
						
						$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
						$query_args = array(
							'paged' => $paged,
						);
						$the_query = new WP_Query( $query_args ); 

						$key = esc_html($s, 1); $count = $allsearch->post_count; 
						_e('Foram encontrados <strong>'); echo $count . '&#32;resultado(s)' ; _e('</strong> com o termo'); _e('<strong class="search-terms"><span>"'); echo $key; _e('"</span></strong>'); wp_reset_query(); ?>. Se preferir, filtre sua busca:
					</p>
                </div>
			</div>
			<div class="row">
				<div class="col-12 col-md-12 col-lg-6">
					<form method="get" class="searchform search-group" action="<?php echo esc_url( home_url( '/' ) ); ?>/">
                        <input id="search" type="text" value="" placeholder="Procure aqui..." name="s" class="s search" />
                        <input type="submit" class="search-submit btn btn-custom green" value="Buscar" />
                        <input type="hidden" value="post" name="post_type" id="post_type" />
                    </form>
				</div>
			</div>

			<div class="results">
				<div class="row">
					<?php while (have_posts()) : the_post(); ?>

						<div class="col-12">
							<div class="row result-item">
								<div class="col-12 col-md-6 col-lg-3">
									<figure>
										<?php  if ( has_post_thumbnail() ) { the_post_thumbnail( 'thumb-blog' ); } ?>
									</figure>
								</div>
								<div class="col-12 col-md-6 col-lg-9">
									<h3><?php echo excerpt_title(6) ?></h3>
									<p><strong><?php the_time('d/m/Y'); ?> - <span>por</span> <?php the_author(); ?></strong></p>
									<p class="excerpt"><?php echo excerpt(50) ?></p>
                                    <a href="<?php the_permalink(); ?>" class="btn btn-custom purple">Saiba Mais</a>
								</div>
							</div>
						</div>

					<?php endwhile; the_posts_pagination(); ?>
				</div>
			</div>
           

          

          
        </div>
       
    </section> -->


</main>
	
<?php get_footer(); ?>



