<?php

get_header();

?>

<main class="inner">



    <div class="breadcrumb-custom">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ul>
                        <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
                        <li><a href="<?php echo esc_url( home_url( '/blog' ) ); ?>">Blog</a></li>
                        <li><?php the_title(); ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <div class="container single-blog">
            <div class="row">
                <div class="col-12">
                    <figure>
                    <?php	if ( has_post_thumbnail() ) { the_post_thumbnail( 'banner-blog' ); } ?>
                    </figure>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <h2><?php the_title(); ?></h2>
                    <p><strong><span>por</span> <?php the_author(); ?> <span>em</span> <?php the_time('d/m/Y'); ?></strong></p>
                    <p class="category"> <?php $categories = get_the_category();if ( ! empty( $categories ) ) {echo esc_html( $categories[0]->name ) ;} ?></p>
                </div>

                <div class="col-12">
                    <div class="post-content">
                        <?php the_content(); ?>
                    </div>
                </div>

                <div class="col-12">
                    <div class="button-line">
                        <a href="javascript:void(0)" onclick="history.back();" class="btn btn-custom purple btn-back">Voltar</a>
                        <ul class="post-tags">
                            <?php wp_list_categories( array(
                                'title_li' => '',
                                'taxonomy' => 'post_tag',
                                'number'   => '3',
                            ) ); ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    <?php endwhile; else : ?> <?php endif; ?>


    <section class="related related-blog">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h3>Veja também:</h4>
				</div>
			</div>
			<div class="row">
                <?php
                    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                    //$categoria = $categoria[0]->name;
                    $query_args = array(
                            'offset'                 => 0,
                            'post_type'              => 'post',
                            'posts_per_page'         => '4',
                            'order'                  => 'DESC',
                            'category__in'           => wp_get_post_categories( $post->ID ),
                            'orderby'                =>'rand',
                            'paged' => $paged
                    );
                    $the_query = new WP_Query( $query_args );   
                ?>
                <?php  if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <div class="col-12 col-md-6 col-lg-3">
                        <h4><?php echo excerpt_title(6) ?></h4>
                        <p><?php echo excerpt(18) ?></p>
                        <a href="<?php the_permalink(); ?>" class="btn btn-custom purple">Saiba Mais</a>
                    </div>
                <?php endwhile; ?> <?php wp_reset_postdata(); ?> <?php endif; ?> 
			</div>
		</div>
	</section>



	
	

	<?php include 'assets/general/floating-whatsapp.php';?>

</main>





<?php get_footer(); ?> 

