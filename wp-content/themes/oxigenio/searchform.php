<?php $s = get_search_query(); ?>
<form method="get" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>/">
	<div class="row">
		<div class="col-12">
			<?php
                $blog_id = get_current_blog_id();
                if ( 1 == $blog_id ) {
                ?>

				<input type="text" value="" placeholder="<?php _e("Pesquise...", 'basic'); ?>" name="s" class="search-input" />

                <?php } else { ?>

                    <input type="text" value="" placeholder="<?php _e("Search...", 'basic'); ?>" name="s" class="search-input" />

            <?php } ?>
	    	
	    </div>
	    <div class="col-12">
	    	<input type="submit" class="btn btn-radius" value="<?php echo esc_attr_x( 'Pesquisar', 'submit button' ) ?>" />
	    </div>
	    <input type="hidden" value="blog" name="post_type" id="post_type" />
	</div>
</form>
