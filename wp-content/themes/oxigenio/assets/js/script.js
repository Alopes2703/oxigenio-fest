(function($) {

    $(window).on('load', function () {
        $('.loader').fadeOut();

      
        setTimeout(function(){
            $('.home .wrap h2').addClass('active');
        }, 1000);

        setTimeout(function(){
            $('.home .wrap .presents').addClass('active');
        }, 1300);

        setTimeout(function(){
            $('figure').addClass('active');
        }, 2000);

        setTimeout(function(){
            $('.bands').addClass('active');
        }, 2500);

        setTimeout(function(){
            $('header').addClass('active');
            $('.logo-header h1').addClass('active');
        }, 3300);

        setTimeout(function(){
            $('.scroll').addClass('active');
        }, 4000);
    });

	
	$(document).ready(function() {





        
        

        $('.btn-menu').click(function(){
            $(this).toggleClass('active');
            $('header').toggleClass('active');
            $('header .container').toggleClass('active');
            $('header').toggleClass('menu-open');
            $('.menu-trigger').toggleClass('active');
        });

        if(jQuery(window).width()<769){


            $('header .navbar ul li a').click(function(){
                $('.btn-menu').removeClass('active');
                $('header').removeClass('active');
                $('header .container').removeClass('active');
                $('header').removeClass('menu-open');
                $('.menu-trigger').removeClass('active');
            });
    
        }



        // Header Animation

        $(window).scroll(function () {
            var scroll = $(window).scrollTop();

            if($(window).width()>768){


                if (scroll > 250) {
                    $('header').addClass('fixed');
                } else {
                    $('header').removeClass('fixed');
                }

            }

           
        });

       

    
        // Easing Page Scroll
    
    
        $(function() {
            $('a.page-scroll').bind('click', function(event) {
                var $anchor = $(this);
                $('html, body').stop().animate({
                    scrollTop: $($anchor.attr('href')).offset().top
                }, 1500, 'easeInOutExpo');
                event.preventDefault();
            });
        });
    
        jQuery.easing.jswing = jQuery.easing.swing;
        jQuery.extend(jQuery.easing, {
            def: "easeOutQuad",
            easeInOutExpo: function(e, f, a, h, g) {
                if (f == 0) {
                    return a
                }
                if (f == g) {
                    return a + h
                }
                if ((f /= g / 2) < 1) {
                    return h / 2 * Math.pow(2, 10 * (f - 1)) + a
                }
                return h / 2 * (-Math.pow(2, -10 * --f) + 2) + a
            },
    
        });
    
        

        // Custom Select

        //$('select').selectric();


        $('.wpcf7-list-item').click(function(){
            $(this).toggleClass('selected');

            if($(this).hasClass('selected')) {
                $(this).find('input').prop('checked', true);
            } else {
                $(this).find('input').prop('checked', false);
            }
            
        });


        $('.show-form').click(function(){
            $('.hidden-form .wpcf7').slideToggle('fast');
            $('section.your-band').toggleClass('small-pad');
        });


        // Slides


        $('.slide-logos').slick({
            autoplay: true,
            autoplaySpeed: 3000,
            dots: false,
            infinite: true,
            arrows: false,
            slidesToShow: 9,
            slidesToScroll: 1,
            fade: false,
            speed: 1000,
            pauseOnFocus: false,
            pauseOnHover: false,
            responsive: [
                {
                    breakpoint: 769,
                        settings: {
                        arrows: false,
                        slidesToShow: 4,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 501,
                        settings: {
                        arrows: false,
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                }
            ]
        });



        var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            spOptions = {
            onKeyPress: function(val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };
        
        $('.tel').mask(SPMaskBehavior, spOptions);


       
    
    });
	
})( jQuery );


