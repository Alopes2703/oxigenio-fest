# Instalação Wordpress Padrão VitaminaWeb

[![N|Solid](http://www.vitaminaweb.com.br/wp-content/uploads/2017/01/logo_vitaminaweb.png)](https://www.vitaminaweb.com.br)

Esta é a instalação padrão do Wordpress que deve ser utilizada nos projetos da VitaminaWeb.

**Esta instalação deve ser utilizada em todos os projetos Wordpress, sem excessão, pois nesta instalação constam os plugins obrigatórios e recomendados para os nossos projetos.**

# Plugins que fazem parte da instalação

  - **Advanced Custom Fields** : Criação de campos customizados no Wordpress (Deve ser substituído pela versão PRO após a criação do ambiente).
  - **Akismet Anti-Spam** : Anti-Spam para os comentários do Wordpress.
  - **Analytify - Google Analytics Dashboard** : Integração com Google Analytics e disponibilização de um relatório amigável para visualização do cliente.
  - **Contact Form 7** : Criação de formulários de contato.
  - **Contact Form CFDB7** : Armazenamento dos leads cadastrados com possibilidade de extração em CSV.
  - **Custom Post Type UI** : Criação de Posts Types customizados para tornar a utilização do painel mais amigável para o usuário.
  - **Integração RD Station** : Integração padrão com o RD Station.
  - **PushCrew** : Integração com o PushCrew para envio de webpushs.
  - **W3 Total Cache** : Plugin de Cache.
  - **Wordfence Security** : Plugin de Segurança com anti-virus, firewall e bloqueio de tentativas de brute force.
  - **WP User Avatar** : Plugin para inclusão de foto no perfil do usuário do Wordpress para os casos de necessidade de exibir a foto do criador do conteúdo.
  - **Yoast SEO** : Plugin de SEO, já integrado com o WP Total Cache.
 
# Banco de Dados

O arquivo de banco de dados está disponível na pasta Downloads deste repositório. **É recomendado o uso deste banco de dados na instalação do ambiente pois algumas configurações de integração entre plugins já foram realizadas**.

# Importante

Todos os plugins estão desativados na instalação, deve ativar somente os plugins que serão utilizados no projeto. **Porém, existem plugins que são obrigatórios e devem ser ativados em todas as instalações de Wordpress da VitaminaWeb. São eles:**

  - **Akismet Anti-Spam** : Anti-Spam para os comentários do Wordpress.
  - **Analytify - Google Analytics Dashboard** : Integração com Google Analytics e disponibilização de um relatório amigável para visualização do cliente.
  - **Contact Form 7** : Criação de formulários de contato.
  - **Contact Form CFDB7** : Armazenamento dos leads cadastrados com possibilidade de extração em CSV.
  - **W3 Total Cache** : Plugin de Cache.
  - **Wordfence Security** : Plugin de Segurança com anti-virus, firewall e bloqueio de tentativas de brute force.
  - **Yoast SEO** : Plugin de SEO, já integrado com o WP Total Cache.
  
# .gitignore

Não foram inseridos arquivos no .gitignore deste projeto. O projeto deve sempre ser utilizado completo.

**Em caso de dúvida ou sugestões, procurar: Fábio Figueredo, Ericc Antunes ou Rodrigo Oliveira.**